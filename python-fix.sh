if [ -f /usr/bin/python3 ] && [ ! -f /usr/bin/python ]; then 
  ln --symbolic /usr/bin/python3 /usr/bin/python; 
fi
